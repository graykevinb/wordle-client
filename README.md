# wordle-client

<hr>

## Project Layout

The API will have the following features:

- Word of the day
- Leader board (for the word of the day)
- Online gaming via web-sockets
- Team profiles


<details><summary><strong>Word-of-the-day</strong></summary>
The word of the day is implemented using a call to an external API to get the day of the year. When the day changes, so does the word (it will be chosen randomly). The client can then query our API and get the word of the day to play. 
</details>

<details><summary><strong>Leader Board</strong></summary> 
The leader board should only be able to be accessed when using the word of the day. When a user plays using the word of the day their score should be sent to the server and stored. The leader board should update each day and should include all teams (not just a given users team). 
</details>


<details><summary><strong>Online gaming via web-sockets</strong></summary>


#### Description
The game is a simple timed match that includes users from different/same teams. Once the game starts, the users race to solve the word.

An alternate form of the game could remove the time element and simply be a room to play together and see who guesses a given word in the least amount of guesses

#### Protocol

##### The Lobby
The user enters a game room by querying the 'lobby' to retrieve all active rooms (which will give the user a name and an id). The user can then enter the room using a `ws` request by using the id _or_ can create a new room using a `post` request.

##### Game Room
The user can send a `ws` 'status' message to the server to inform all other users that they are ready. When all users have submitted that they are ready, the room will close and the game will start. When a game room is created a word is chosen within the `GameController` (which facilitates game play). The official-word (which remains hidden to the user) is then sent to each client for validation.

##### Game play
Each time a user makes a guess, the client will handle the logic of displaying the appropriate response to the guess. The server will also verify the guess and generate a response for all active players. If the word is correct, the status message should inform the users that the game is over, otherwise, it should hold the properties of each letter in the guess (in the word, not in the word, correct position). This will allow for developers to implement a display for other users so that players can see the other players real-time progress.
</details>

## Creating requests

<hr>

Since we are creating a client library for the API, it will be more efficient to use
typed requests and send and receive serialized objects.


Always start by creating a data class that represents the json response _or_ what represents
the parameters that you need to send to the server. 
```
@Serializable
data class ReceivingObject(
    val key1: String, 
    val key2: String, 
    val key3: String
)

```

After that you can create a request: 

```
suspend fun getRequest() {
    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }
    val response : ReceivingObject = client.get("http://localhost:8080/url")
    println(response.key1)
    println(response.key2)
    println(response.key3)
}
```

Post requests can send the object as a body-

You can switch the response to be an HttpResponse:

```
suspend fun postRequest() {
    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }
    val response : HttpResponse = client.post("http://localhost:8080/url") {
        contentType(ContentType.Application.Json)       // we need to specify the content type otherwise we get errors
        body = ReceivingObject("value1", "value2", "value3")
    }
}
```