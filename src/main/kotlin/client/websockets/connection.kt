package client.websockets

import client.dataclass.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.websocket.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.lang.NumberFormatException
import kotlin.concurrent.thread


/**
 * Handles a websocket connection to the wordle server. Contains useful methods to make responses to the server.
 * Can also be attached to with a ClientGameConnectionObserver to subscribe to responses from the server.
 */
class ClientGameConnection(
    private val host: String,
    private val port: Int,
    private val path: String,
    private val username: String,
    private val teamName: String
) {
    private val observers: MutableList<ClientGameConnectionObserver> = ArrayList()
    private lateinit var socket: WebSocketSession

    /**
     * Attach the observer to subscribe to server responses.
     */
    fun attach(observer: ClientGameConnectionObserver) {
        observers.add(observer);
    }

    /**
     * Attach the observer to subscribe to server responses.
     */
    fun detach(observer: ClientGameConnectionObserver) {
        observers.remove(observer)
    }

    /**
     * Connect to the server and open the websocket.
     */
    fun connect() {
        val client = HttpClient(CIO) {
            install(WebSockets) {

            }
        }

        try {
            runBlocking {
                client.webSocket(method = HttpMethod.Get, host = host, port = port, path =
                path) {
                    socket = this
                    val authResponse = AuthResponse(username, teamName)
                    send(Json.encodeToString(authResponse))
                    try {
                        while (true) {
                            val frame = incoming.receive()
                            if (frame is Frame.Text) {
                                val resp = Json.decodeFromString<GameResponse>(frame.readText())
                                handleServerResponse(resp)
                            } else if (frame is Frame.Close) {
                                observers.forEach {observer -> observer.connectionClosed()}
                            }
                        }
                    } catch (e: ClosedReceiveChannelException) {
                        observers.forEach {observer -> observer.connectionClosed()}
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        observers.forEach {observer -> observer.connectionClosed()}
                    }
                }
                client.close()
            }
        } catch (e: Throwable) {
            throw CannotConnect()
        }
    }

    /**
     * Sends a client response to the server.
     */
    private suspend fun sendClientResponse(response: GameClientResponse) {
        val resp = ClientResponse(System.currentTimeMillis(), response)
        socket.send(Json.encodeToString(resp))
    }

    /**
     * Create a room with the given info.
     */
    fun createRoom(name: String, wordLength: Int, numGuesses: Int) {
        runBlocking {
            val resp = CreateRoomResponse(name, wordLength, numGuesses)
            sendClientResponse(resp);
        }
    }

    /**
     * Join a room with the given ID. If that room doesn't exist, is full, or is already started an ErrorResponse will be sent.
     */
    fun joinRoom(id: Int) {
        runBlocking {
            val resp = JoinRoomResponse(id)
            sendClientResponse(resp);
        }
    }

    /**
     * Leave the lobby and close the connection.
     */
    fun leaveLobby() {
        runBlocking {
            val resp = LeaveLobbyResponse
            sendClientResponse(resp)
        }
    }

    /**
     * Set whether you're ready or not.
     */
    fun setReady(ready: Boolean) {
        runBlocking {
            val resp = SetReadyResponse(ready)
            sendClientResponse(resp);
        }
    }

    /**
     * Leave the current room and go back to the lobby.
     */
    fun leaveRoom(reason: String) {
        runBlocking {
            val resp = LeaveResponse(reason)
            sendClientResponse(resp);
        }
    }

    /**
     * Guess the given word. If the word is valid and the player has guesses remaining then memberGuessed
     * will be triggered, otherwise corresponding errors will be triggered.
     */
    fun guessWord(word: String) {
        runBlocking {
            val resp = GuessWordResponse(word)
            sendClientResponse(resp)
        }
    }

    /**
     * Attempt to start the game. If this member is not the owner of this room or not all the players are ready then an
     * ErrorResponse will be triggered.
     */
    fun startGame() {
        runBlocking {
            val resp = StartResponse
            sendClientResponse(resp)
        }
    }

    /**
     * Set the word length of the game if you're the owner of the room.
     */
    fun setWordLength(length: Int) {
        runBlocking {
            val resp = SetWordLengthResponse(length)
            sendClientResponse(resp);
        }
    }

    /**
     * Set the number of guesses of the game if you're the owner of the room.
     */
    fun setNumGuesses(numGuesses: Int) {
        runBlocking {
            val resp = SetNumGuessesResponse(numGuesses)
            sendClientResponse(resp)
        }
    }

    /**
     * Handles all server responses.
     */
    private fun handleServerResponse(gameResponse: GameResponse) {
        when (gameResponse.response) {
            is LobbyResponse -> handleLobbyResponse(gameResponse.response as LobbyResponse)
            is RoomResponse -> handleRoomResponse(gameResponse.response as RoomResponse)
            is ErrorResponse -> observers.forEach { it.errorSent(gameResponse.response as ErrorResponse) }
        }
    }

    /**
     * Handles all lobby responses.
     */
    private fun handleLobbyResponse(lobbyResponse: LobbyResponse) {
        when(lobbyResponse) {
            is RoomCreated -> observers.forEach { it.roomCreated(lobbyResponse) }
            is RoomChanged -> observers.forEach { it.roomChanged(lobbyResponse) }
            is MemberJoinedLobby -> observers.forEach { it.memberJoinedLobby(lobbyResponse) }
            is MemberLeftLobby -> observers.forEach { it.memberLeftLobby(lobbyResponse) }
            is RoomInfos -> observers.forEach { it.roomInfos(lobbyResponse) }
            is JoinedLobby -> observers.forEach { it.joinedLobby(lobbyResponse) }
            is RoomDeleted -> observers.forEach { it.roomDeleted(lobbyResponse) }
        }
    }

    /**
     * Handles all room responses.
     */
    private fun handleRoomResponse(roomResponse: RoomResponse) {
        when(roomResponse) {
            is JoinResponse -> observers.forEach { it.roomJoined(roomResponse) }
            is FullResponse -> observers.forEach { it.roomIsFull(roomResponse) }
            is GameReadyResponse -> observers.forEach { it.gameIsReady(roomResponse) }
            is GameNotReadyResponse -> observers.forEach { it.gameIsNotReady(roomResponse) }
            is StartedResponse -> observers.forEach { it.gameStarted() }
            is EndResponse -> observers.forEach { it.gameEnded(roomResponse) }
            is MemberJoinedResponse -> observers.forEach { it.memberJoinedRoom(roomResponse) }
            is MemberReadyResponse -> observers.forEach { it.memberSetReady(roomResponse) }
            is MemberLeftResponse -> observers.forEach { it.memberLeftRoom(roomResponse) }
            is WordLengthChangedResponse -> observers.forEach { it.wordLengthChanged(roomResponse) }
            is NumGuessesChangedResponse -> observers.forEach { it.numGuessesChanged(roomResponse) }
            is MemberGuessedResponse -> observers.forEach { it.memberGuessed(roomResponse) }
            is MemberLostResponse -> observers.forEach { it.memberLost(roomResponse) }
            is InvalidWordResponse -> observers.forEach { it.invalidWord(roomResponse) }
            is WordNotInWordListResponse -> observers.forEach { it.wordNotInWordlist(roomResponse) }
        }
    }
}

/**
 * Will be thrown if the server cannot be connected to.
 */
class CannotConnect : Exception() {

}

/**
 * Observer to listen for all responses made by the wordle server websocket.
 */
interface ClientGameConnectionObserver {
    /**
     * Called when a member guesses.
     */
    fun memberGuessed(roomResponse: MemberGuessedResponse)

    /**
     * Called when the number of allowed guesses changes.
     */
    fun numGuessesChanged(roomResponse: NumGuessesChangedResponse)

    /**
     * Called when the word length is changed.
     */
    fun wordLengthChanged(roomResponse: WordLengthChangedResponse)

    /**
     * Called when a member of your current room leaves.
     */
    fun memberLeftRoom(roomResponse: MemberLeftResponse)

    /**
     * Called when a member in your current room changes their ready status.
     */
    fun memberSetReady(roomResponse: MemberReadyResponse)

    /**
     * Called when a member joins your current room.
     */
    fun memberJoinedRoom(roomResponse: MemberJoinedResponse)

    /**
     * Called when the game ends.
     */
    fun gameEnded(roomResponse: EndResponse)

    /**
     * Called when the room you're currently in is ready to start.
     */
    fun gameIsReady(roomResponse: GameReadyResponse)

    /**
     * Called when the room you're currently starts.
     */
    fun gameStarted();

    /**
     * Called when the room you're trying to join is full.
     */
    fun roomIsFull(roomResponse: FullResponse)

    /**
     * Called when you join a room.
     */
    fun roomJoined(roomResponse: JoinResponse)

    /**
     * Called with various error messages.
     */
    fun errorSent(response: ErrorResponse)

    /**
     * Called when a new room is created in your current lobby.
     */
    fun roomCreated(lobbyResponse: RoomCreated)

    /**
     * Called when a room in your current lobby changes in some way, such as members, settings, status, etc.
     */
    fun roomChanged(lobbyResponse: RoomChanged)

    /**
     * Called when a member joins the lobby you're currently in.
     */
    fun memberJoinedLobby(lobbyResponse: MemberJoinedLobby)

    /**
     * Called when a member leaves your current lobby.
     */
    fun memberLeftLobby(lobbyResponse: MemberLeftLobby)

    /**
     * Deprecated at the moment.
     */
    fun roomInfos(lobbyResponse: RoomInfos)

    /**
     * Called when you join a lobby.
     */
    fun joinedLobby(lobbyResponse: JoinedLobby)

    /**
     * Called when a room in your current lobby is deleted.
     */
    fun roomDeleted(lobbyResponse: RoomDeleted)

    /**
     * Called when the connection to the server is closed.
     */
    fun connectionClosed()

    /**
     * Called when your current room is no longer ready.
     */
    fun gameIsNotReady(roomResponse: GameNotReadyResponse)

    /**
     * Called when a member in your current room loses by using up their guesses.
     */
    fun memberLost(roomResponse: MemberLostResponse)

    /**
     * Called when you attempt to guess a word that doesn't exist.
     */
    fun wordNotInWordlist(roomResponse: WordNotInWordListResponse)

    /**
     * Called when you attempt to guess an invalid word.
     */
    fun invalidWord(roomResponse: InvalidWordResponse)


}

/**
 * Quick CLI to run initial tests on the socket connection. Serves as a simple example.
 */
class ClientCLI : ClientGameConnectionObserver {
    var connected = false
    lateinit var connection: ClientGameConnection

    fun start() {
        connection = connect()
        while (true) {
            handleLobbyCommands(connection)
        }
    }

    private fun handleLobbyCommands(connection: ClientGameConnection) {
        println("Enter a command: ")
        when (readLine().toString().trim()) {
            "c" -> createRoom(connection)
            "j" -> joinRoom(connection)
            "r" -> setReady(connection)
            "lr" -> leaveRoom(connection)
            "ll" -> leaveLobby(connection)
            "g" -> guessWord(connection)
            "s" -> startGame(connection)
            "sw" -> setWordLength(connection)
            "sg" -> setGuessNumber(connection)
            else -> {
                println("Unrecognized command!")
                handleLobbyCommands(connection)
            }
        }
    }

    private fun setGuessNumber(connection: ClientGameConnection) {
        connection.setNumGuesses(6)
    }

    private fun setWordLength(connection: ClientGameConnection) {
        connection.setWordLength(5)
    }

    private fun startGame(connection: ClientGameConnection) {
        connection.startGame()
    }

    private fun guessWord(connection: ClientGameConnection) {
        val guess = getStringValueFromUser("Please enter guess: ")
        connection.guessWord(guess)
    }

    private fun leaveLobby(connection: ClientGameConnection) {
        connection.leaveLobby()
    }

    private fun leaveRoom(connection: ClientGameConnection) {
        connection.leaveRoom("Because")
    }

    private fun setReady(connection: ClientGameConnection) {
        println("Enter whether ready (t/f):")
        var c = ""
        while (c != "f" && c != "t") {
            c = readLine().toString().trim().lowercase()
            connection.setReady(c == "t")
        }
    }

    private fun joinRoom(connection: ClientGameConnection) {
        val id = getIntValueFromUser("Please enter a valid room id: ")
        connection.joinRoom(id)
    }

    private fun createRoom(connection: ClientGameConnection) {
        val name = getStringValueFromUser("Please enter a valid room name: ")
        val wordLength = getIntValueFromUser("Please enter a valid word length: ")
        val numGuesses = getIntValueFromUser("Please enter a valid number of guesses: ")
        connection.createRoom(name, wordLength, numGuesses)
    }

    private fun getStringValueFromUser(prompt: String) : String {
        var ret = ""
        while (ret == "") {
            println(prompt)
            ret = readLine().toString().trim()
        }
        return ret
    }

    private fun getIntValueFromUser(prompt: String) : Int {
        var ret = -1
        while (ret < 0) {
            println(prompt)
            try {
                ret = readLine()?.trim()?.toInt() ?: -1
            } catch (e: NumberFormatException) {}
        }
        return ret
    }

    private fun connect() : ClientGameConnection {
        var username = ""
        var teamName = ""
        while (username == "" || teamName == "") {
            println("Please enter a username: ")
            username = readLine().toString().trim()

            println("Please enter a team name: ")
            teamName = readLine().toString().trim()
        }

        val connection = ClientGameConnection("wordle.mwdev.org", 80, "/time-match", username,
            teamName)
        connection.attach(this)
        thread(start = true) {
            runBlocking {
                try {
                    connection.connect()
                    connected = true
                } catch (e: CannotConnect) {
                    println("Cannot connect to the wordle server. Please make sure that it is running.")
                }
            }
        }
        return connection
    }

    override fun memberGuessed(roomResponse: MemberGuessedResponse) {
        println(roomResponse)
    }

    override fun numGuessesChanged(roomResponse: NumGuessesChangedResponse) {
        println(roomResponse)
    }

    override fun wordLengthChanged(roomResponse: WordLengthChangedResponse) {
        println(roomResponse)
    }

    override fun memberLeftRoom(roomResponse: MemberLeftResponse) {
        println(roomResponse)
    }

    override fun memberSetReady(roomResponse: MemberReadyResponse) {
        println(roomResponse)
    }

    override fun memberJoinedRoom(roomResponse: MemberJoinedResponse) {
        println(roomResponse)
    }

    override fun gameEnded(roomResponse: EndResponse) {
        println(roomResponse)
    }

    override fun gameIsReady(roomResponse: GameReadyResponse) {
        println("Game is ready!")
    }

    override fun gameStarted() {
        println("Game Started")
    }

    override fun roomIsFull(roomResponse: FullResponse) {
        println(roomResponse)
    }

    override fun roomJoined(roomResponse: JoinResponse) {
        println(roomResponse)
    }

    override fun errorSent(response: ErrorResponse) {
        println(response)
    }

    override fun roomCreated(lobbyResponse: RoomCreated) {
        println(lobbyResponse)
    }

    override fun roomChanged(lobbyResponse: RoomChanged) {
        println(lobbyResponse)
    }

    override fun memberJoinedLobby(lobbyResponse: MemberJoinedLobby) {
        println(lobbyResponse)
    }

    override fun memberLeftLobby(lobbyResponse: MemberLeftLobby) {
        println(lobbyResponse)
    }

    override fun roomInfos(lobbyResponse: RoomInfos) {
        println(lobbyResponse)
    }

    override fun joinedLobby(lobbyResponse: JoinedLobby) {
        println(lobbyResponse)
    }

    override fun roomDeleted(lobbyResponse: RoomDeleted) {
        println(lobbyResponse)
    }

    override fun connectionClosed() {
        println("Connection closed!")

        connected = false
    }

    override fun gameIsNotReady(roomResponse: GameNotReadyResponse) {
        println("Game is not ready!")
    }

    override fun memberLost(roomResponse: MemberLostResponse) {
        println(roomResponse)
    }

    override fun wordNotInWordlist(roomResponse: WordNotInWordListResponse) {
        println(roomResponse)
    }

    override fun invalidWord(roomResponse: InvalidWordResponse) {
        println(roomResponse)
    }

}


fun main() {
    val cli = ClientCLI()
    cli.start()
}