package client.dataclass

import kotlinx.serialization.Serializable

/**
 * @author harwoodl
 * Enumeration to describe different properties for a cell
 */
@Serializable
enum class Property(
    /**
     * @author harwoodl
     * @return the style describing the property as a String
     */
    val style: String,
    /**
     * @author keanej
     * @return The ranking of the property (in terms of information given)
     */
    var ranking: Int
) {
    IN_WORD("incorrect-position", 2), CORRECT_LOCATION(
        "correct-position",
        3
    ),
    NOT_IN_WORD("not-in-word", 1), UNSUBMITTED("unsubmitted-letter", 0);

    init {
        ranking = ranking
    }
}



@Serializable
enum class GameStatus {
    IN_PROGRESS,
    GAME_OVER,
    START,
    READY,
    NOT_READY
}


@Serializable
data class RoomInfo(
    val id: Int,
    val numMembers: Int,
    val maxMembers: Int,
    val full: Boolean,
    val name: String,
    val status: GameStatus,
    val wordLength: Int,
    val numGuesses: Int,
    val members: List<MemberInfo>,
    val owner: String
)

@Serializable
data class MemberInfo(
    val username: String,
    val teamName: String,
    val ready: Boolean
)

@Serializable
data class LobbyInfo(
    val numMembers: Int,
    val members: List<String>,
    val numRooms: Int,
    val roomInfos: List<RoomInfo>
)