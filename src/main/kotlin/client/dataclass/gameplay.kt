package client.dataclass

import kotlinx.serialization.Serializable

/**
 * @author harwoodl
 * This file should contain all data classes that are used for transferring data
 *          between the server and client within requests.
 *
 */

@Serializable
data class LoginRequest(
    val username: String,
    val teamId: String
)

@Serializable
data class GameEntry(
    val username: String,
    val teamId: String,
    val score: Int,
    val word: String,
    val properties: List<Pair<Char, Property>>
)

@Serializable
data class Entry(val team: Team, val user: User, val numGuesses: Int, val word: String)

@Serializable
data class Team(val teamName: String, val id: String) {

    val users = HashMap<String, User>()
}

@Serializable
data class User(
    val username: String,
    val teamId: String) {

    override fun toString(): String {
        return "~Username: $username ~TeamID: $teamId~"
    }
}

@Serializable
data class OfficialWord(
    val word: String
)