package client.dataclass

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Class sent back to all members
 */
@Serializable
data class ClientResponse(
    val timeStamp : Long,
    val gameClientResponse : GameClientResponse
)

@Serializable
sealed class GameClientResponse

@Serializable
sealed class GameClientLobbyResponse : GameClientResponse()

@Serializable
@SerialName("join_room")
data class JoinRoomResponse (
    val roomID : Int
) : GameClientLobbyResponse()

@Serializable
@SerialName("leave_lobby")
object LeaveLobbyResponse : GameClientLobbyResponse()

@Serializable
@SerialName("create_room")
data class CreateRoomResponse (
    val name : String,
    val wordLength : Int,
    val numGuesses : Int,
) : GameClientLobbyResponse()


@Serializable
sealed class GameClientRoomResponse : GameClientResponse()

@Serializable
@SerialName("set_ready")
data class SetReadyResponse (
    val ready : Boolean
) : GameClientRoomResponse()

@Serializable
@SerialName("leave")
data class LeaveResponse (
    val reason : String
) : GameClientRoomResponse()

@Serializable
@SerialName("guess")
data class GuessWordResponse (
    val word : String
) : GameClientRoomResponse()

@Serializable
@SerialName("start")
object StartResponse : GameClientRoomResponse()

@Serializable
@SerialName("set_word_length")
data class SetWordLengthResponse (
    val length : Int
) : GameClientRoomResponse()

@Serializable
@SerialName("set_num_guesses")
data class SetNumGuessesResponse (
    val numGuesses : Int
) : GameClientRoomResponse()

/**
 * Class sent back to all members
 */
@Serializable
data class GameResponse(
    val timeStamp : Long,
    val response : ParentResponse
)

@Serializable
sealed class ParentResponse

@Serializable
@SerialName("error")
data class ErrorResponse (
    val message : String
) : ParentResponse()

@Serializable
sealed class LobbyResponse : ParentResponse()

@Serializable
@SerialName("room_infos")
data class RoomInfos (
    val roomInfos: List<RoomInfo>
) : LobbyResponse()

@Serializable
@SerialName("room_created")
data class RoomCreated (
    val roomInfo: RoomInfo
) : LobbyResponse()

@Serializable
@SerialName("joined_lobby")
data class JoinedLobby (
    val lobbyInfo: LobbyInfo
) : LobbyResponse()

@Serializable
@SerialName("room_changed")
data class RoomChanged (
    val roomInfo: RoomInfo
) : LobbyResponse()

@Serializable
@SerialName("room_deleted")
data class RoomDeleted (
    val roomInfo: RoomInfo
) : LobbyResponse()

@Serializable
@SerialName("member_joined_lobby")
data class MemberJoinedLobby (
    val username: String
) : LobbyResponse()

@Serializable
@SerialName("member_left_lobby")
data class MemberLeftLobby (
    val username: String
) : LobbyResponse()

@Serializable
sealed class RoomResponse : ParentResponse()

@Serializable
@SerialName("started")
object StartedResponse : RoomResponse()

@Serializable
@SerialName("join")
data class JoinResponse (
    val RoomInfo : RoomInfo
) : RoomResponse()

@Serializable
@SerialName("full")
object FullResponse : RoomResponse()

@Serializable
@SerialName("game_ready")
object GameReadyResponse : RoomResponse()

@Serializable
@SerialName("game_not_ready")
object GameNotReadyResponse : RoomResponse()

@Serializable
@SerialName("end")
data class EndResponse(
    val winner: String?,
    val word: String
) : RoomResponse()

@Serializable
@SerialName("member_joined")
data class MemberJoinedResponse (
    val user : String
) : RoomResponse()

@Serializable
@SerialName("member_ready")
data class MemberReadyResponse (
    val user : String,
    val ready : Boolean
) : RoomResponse()

@Serializable
@SerialName("member_left")
data class MemberLeftResponse (
    val user : String,
) : RoomResponse()

@Serializable
@SerialName("word_length_changed")
data class WordLengthChangedResponse (
    val wordLength : Int
) : RoomResponse()

@Serializable
@SerialName("num_guesses_changed")
data class NumGuessesChangedResponse (
    val numGuesses : Int
) : RoomResponse()

@Serializable
@SerialName("guessed")
data class MemberGuessedResponse (
    val user : String,
    val guess : List<Property>,
    val guessNumber : Int
) : RoomResponse()

@Serializable
@SerialName("word_not_in_wordlist")
data class WordNotInWordListResponse (
    val word : String
) : RoomResponse()

@Serializable
@SerialName("invalid_word")
data class InvalidWordResponse (
    val word : String
) : RoomResponse()

@Serializable
@SerialName("member_lost")
data class MemberLostResponse (
    val user : String
) : RoomResponse()

@Serializable
data class AuthResponse(
    val userName : String,
    val teamName : String
)
