package client.core

import client.dataclass.OfficialWord
import client.dataclass.GameEntry
import client.dataclass.LoginRequest
import client.dataclass.Property
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking

class APIClient {
    companion object {

        /**
         * @author harwoodl
         * Returns a random "official" word from database in callback
         */
        fun getRandomWord(callback : (word: String) -> Unit) {
            client.get("/random-word") { client, url ->
                runBlocking {
                    val response: OfficialWord = client.get(url)
                    callback(response.word)
                }
            }
        }

        /**
         * @author harwoodl
         * Returns a random "official" word from database w/o callback
         */
        fun getRandomWord() : String? {
            var ret : String? = null
            client.get("/random-word") { client, url ->
                runBlocking {
                    val response: OfficialWord = client.get(url)
                    ret = response.word
                }
            }
            return ret
        }

        /**
         * @author kevingray, harwoodl
         * Retrieves the official word of the day for the API using callback
         */
        fun getOfficialWord(callback : (word: String) -> Unit) {
            client.get("/official-word") { client, url ->
                runBlocking {
                    val response: OfficialWord = client.get(url)
                    callback(response.word)
                }
            }
        }

        /**
         * @author kevingray, harwoodl
         * Retrieves the official word of the day for the API
         *           Does not use callbacks.
         */
        fun getOfficialWord() : String? {
            var ret: String? = null
            client.get("/official-word") { client, url ->
                runBlocking {
                    val response: OfficialWord = client.get(url)
                    ret = response.word
                }
            }
            return ret
        }

        /**
         * @author harwoodl
         * Get the top ten leaders from the API with callback
         */
        fun getLeaderboard(callback: (List<GameEntry>) -> Unit) {
            client.get("/leaderboard") { client, url ->
                runBlocking {
                    callback(
                        client.get(url)
                    )
                }
            }
        }

        /**
         * @author harwoodl
         * Get the top ten leaders from the API without callback
         */
        fun getLeaderboard() : List<GameEntry>? {
            var ret: List<GameEntry>? = null
            client.get("/leaderboard") { client, url ->
                runBlocking {
                    ret =  client.get(url)
                }
            }
            return ret
        }

        /**
         * @author harwoodl
         * @created 04/15/2022
         * Posts to the leaderboard
         * NOTE:    Assumes that the client is sending the "official" word of the day.
         *          No verification is done.
         */
        fun postToLeaderBoard(username: String, teamId: String, score: Int, word: String, properties: List<Pair<Char, Property>>) : Boolean {
            var success = true
            client.post(
                "/leaderboard",
                GameEntry(
                    username,
                    teamId,
                    score,
                    word,
                    properties
                )) { response ->
                success = (response.status == HttpStatusCode.OK)
            }
            return success
        }

        /**
         * @author harwoodl
         * @created 05/10/2022
         * Grabs word of the day from the board
         */
        fun sync(username: String, teamId: String) : List<Pair<Char, Property>>? {
            var ret: List<Pair<Char, Property>>? = null
            client.get("/sync") { client, url ->
                runBlocking {
                    ret =  client.get(url) {
                        contentType(ContentType.Application.Json)       // we need to specify the content type otherwise we get errors
                        body = LoginRequest(username, teamId)
                    }
                }
            }
            return ret
        }
    }
}