package client

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking

/**
 * @author harwoodl
 * After spending hours trying to figure out a bug with the client
 *          I figured out that the best way to send data from the
 *          client to the server via post requests (or get requests)
 *          is to send a data object that is serialized. This being
 *          said it's gonna have to look a little different.
 *
 * I built this function as an example of what the client should look like
 *          when making requests
 */

fun post(
    url: String,
    data: Any,                                              // this should be a serializable data class
    callback: (response:HttpResponse) -> Unit
) {
    runBlocking {                                           // If you want to have the the person implementing the library
                                                            // handle the threads you can... otherwise you can slap this in
                                                            // a thread as well...
        val client = HttpClient(CIO) {
            install(JsonFeature) {
                serializer = KotlinxSerializer()
            }
        }
        val response: HttpResponse = client.post("${Config.getURL()}/api$url") {
            contentType(ContentType.Application.Json)       // we need to specify the content type otherwise we get errors
            body = data
        }
        callback(response)
        client.close()                                      // close the client when you're done with it
    }
}

/**
 * @author harwoodl
 *
 * Because we need to be receiving typed responses we have
 *          to leave it up to the specific method to say what
 *          the return type is going to be. Therefore, if
 *          we pass in a callback with the client and pre-
 *          formatted url, the problem will be solved, and
 *          we still will have reusable code.
 */
fun get(
    url: String,
    callback: (client: HttpClient, url: String) -> Unit
) {

    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }
    callback(client, "${Config.getURL()}/api$url")  // must pass on client for request
    client.close()                                      // must close client when done
}

object Config {
    val productionMode = true
    private const val productionURL = "http://wordle.mwdev.org" // this should be the entire url with the port#
    val devURL = "http://localhost:8080"

    fun getURL() : String {
        return if (productionMode) {
            productionURL
        } else {
            devURL
        }
    }
}