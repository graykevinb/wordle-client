import client.core.APIClient
import client.dataclass.Entry
import client.get
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*

suspend fun main() {

    val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    testLeaderboard(client)
    testOfficialWord()

}

fun testLeaderboard(client: HttpClient) {
    // Add 5 scores to the leaderboard and make a get request to the leaderboard


}


fun testOfficialWord() {
    // request official word
    APIClient.getOfficialWord {
        println(it)
    }

    // request offical word (no callback)
    println(APIClient.getOfficialWord())
}
